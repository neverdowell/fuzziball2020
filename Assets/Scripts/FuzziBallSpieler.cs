﻿using UnityEngine;

public class FuzziBallSpieler : MonoBehaviour{
    [Header("Balancing Values")]
    public float moveSpeed;
    public float rotationSpeed;

    [Header("Player Input")]
    public KeyCode moveForward = KeyCode.W;
    public KeyCode moveBack = KeyCode.S;
    public KeyCode rotateLeft = KeyCode.A;
    public KeyCode rotateRight = KeyCode.D;
    public KeyCode kick = KeyCode.Space;

    public GameObject playerMesh;

    private Animator animator;


    #region lifecycle
    void Start(){
        animator = playerMesh.GetComponent<Animator>();
    }

    void Update(){
        // rotation
        if (Input.GetKey(rotateRight)){
            transform.Rotate(transform.up * rotationSpeed * Time.deltaTime);
        }

        if (Input.GetKey(rotateLeft)){
            transform.Rotate(transform.up * -rotationSpeed * Time.deltaTime);
        }

        // move
        if (Input.GetKey(moveForward)) { 
            transform.position += transform.forward * moveSpeed * Time.deltaTime;
            animator.SetBool("isRunning", true);
        }

        if (Input.GetKey(moveBack))
        {
            transform.position -= transform.forward * moveSpeed * Time.deltaTime;
            animator.SetBool("isRunning", true);
        }

        if (!Input.GetKey(moveForward) && !Input.GetKey(moveBack))
        {
            animator.SetBool("isRunning", false);
        }

        // kick 
        if (Input.GetKeyDown(kick))
        {
            animator.SetTrigger("kickTrigger");
        }
    }
    #endregion




    #region helper

    #endregion
}
